import { isString } from '@inkylabs/fannypack'
import latexIdx from '@inkylabs/latex-idx'
import {
  handleNode as htmlNode,
  replaceLabels
} from '@inkylabs/pulpweaver-html'
import chalk from 'chalk'
import childProcess from 'child_process'
import esDirname from 'es-dirname'
import glob from 'globby'
import { promises as fs } from 'fs'
import handlebars from 'handlebars'
import { convert as html2Txt } from 'html-to-text'
import fetch from 'node-fetch'
import path from 'path'
import Podcast from 'podcast'
import { v4 as genUuid } from 'uuid'

const __dirname = esDirname()
const TEMPLATE_DIR = path.join(__dirname, 'templates')

const red = m => console.error(chalk.red(m))

const capitalize = s => s[0].toUpperCase() + s.slice(1)

function handleNode (node, ctx) {
  htmlNode(node, ctx)
}

const _templateCache = {}
async function readTemplate (name) {
  if (!(name in _templateCache)) {
    const templatePath = path.join(TEMPLATE_DIR, `${name}.handlebars`)
    const templateText = await fs.readFile(templatePath, 'utf8')
    _templateCache[name] = handlebars.compile(templateText)
  }
  return _templateCache[name]
}

async function readGen (filepath) {
  filepath = path.join('genfiles', `${filepath}.json`)
  const text = await fs.readFile(filepath, 'utf8')
  return JSON.parse(text)
}

async function generateAudioBook ({
  title,
  subtitle,
  description,
  authors,
  audio,
  siteroot,
  email,
  date,
  foreword,
  chapters,
  appendices
}) {
  // If we are prepublication, use now as a date so it's not in the future.
  date = new Date(date)
  const now = new Date()
  if (date > now) date = now

  const author = authors[0].name
  const htmlDescription = description.content
  const txtDescription = html2Txt(description.content)

  // For fields see: https://github.com/maxnowack/node-podcast
  const feed = new Podcast({
    title: `${title}: ${subtitle}`,
    description: `${htmlDescription}`,
    feedUrl: `${siteroot}/audiobook.rss`,
    siteUrl: `${siteroot}/`,
    pubDate: date,
    imageUrl: `${siteroot}/audiobook.jpg`,
    author: author,
    language: 'en',
    itunesAuthor: author,
    itunesSummary: txtDescription,
    itunesOwner: {
      name: author,
      email
    },
    itunesExplicit: false,
    itunesCategory: [{
      text: 'Religion & Spirituality',
      subcats: [{
        text: 'Christianity'
      }]
    }]
  })

  const sections = [
    ...(foreword
      ? [{
          id: 'foreword',
          title: 'Foreword'
        }]
      : []),
    ...chapters,
    ...appendices
  ]
    .map(s => Object.assign({
      fulltitle: s.chapterlabel
        ? `${capitalize(s.type)} ${s.chapterlabel}—${s.title}`
        : s.title
    }, s))
  date = new Date(date.getTime() + sections.length * 1000)
  for (const s of sections) {
    date = new Date(date.getTime() - 1000)
    const url = `${audio.url}/${s.id}.mp3`
    console.log(`Fetching ${url}…`)
    const resp = await fetch(url, { method: 'HEAD' })
    let size = 0
    if (resp.status === 200) {
      size = parseInt(resp.headers.get('content-length'))
    } else {
      console.error(chalk.red(`Could not fetch ${url} (${resp.status})`))
    }

    const sub = s.subtitle ? `: ${s.subtitle}` : ''

    feed.addItem({
      title: s.fulltitle,
      description: `${s.fulltitle}${sub}`,
      url: `${siteroot}/#${s.id}`,
      author: author,
      date,
      itunesAuthor: author,
      itunesTitle: s.fulltitle,
      itunesSummary: `${s.fulltitle}${sub}`,
      itunesImage: `${siteroot}/audiobook.jpg`,
      enclosure: {
        url: `${siteroot}/audio/${s.id}.mp3?v=${audio.version}`,
        size
      }
    })
  }

  return feed.buildXml('  ')
}

function generateRedirects ({ audio, redirects }) {
  const lines = []
  if (audio) {
    lines.push(`/audio/* ${audio.url}/:splat 200\n`)
  }
  for (const r of redirects) {
    lines.push(`${r.src} ${r.dst} ${r.code}\n`)
  }
  return lines.join('')
}

async function writeFiles ({
  title,
  subtitle,
  authors,
  description,
  titlepage,
  copyright,
  dedication,
  foreword,
  appendices,
  chapters,
  indices,
  email,
  date
}, {
  audio,
  googleAnalytics,
  gridsomeConfig,
  favicon,
  staticdir,
  pages,
  head,
  hooks,
  cover,
  links,
  redirects,
  stylesheets,
  packages,
  filedirs,
  siteroot
}, {
  refPrefix,
  idxs
}) {
  const projDir = path.join('..', '..')
  const srcDir = 'src'

  const safecp = async (src, dst) => {
    try {
      await fs.mkdir(path.dirname(dst), { recursive: true })
    } catch (e) {
      if (e.code !== 'EEXIST') throw e
    }
    await fs.copyFile(src, dst)
  }

  try {
    await fs.rmdir(srcDir, { recursive: true })
  } catch (e) {
    if (e.code !== 'ENOENT') throw e
  }
  const filedir = path.join(__dirname, 'files')
  const fps = await glob(path.join(filedir, '**'))
  for (const fp of fps) {
    const rel = path.relative(filedir, fp)
    await safecp(fp, rel)
  }

  if (staticdir) {
    const sd = path.join(projDir, staticdir)
    const fps = await glob(path.join(sd, '**'))
    for (const fp of fps) {
      const dest = path.join('static', path.relative(sd, fp))
      await safecp(fp, dest)
    }
  }

  if (favicon) {
    await fs.copyFile(path.join(projDir, favicon),
      path.join(srcDir, 'favicon.png'))
  }

  if (hooks) {
    await fs.copyFile(path.join(projDir, hooks),
      path.join(srcDir, 'lib', 'hooks.js'))
  }

  for (const s of stylesheets) {
    await safecp(path.join(projDir, s),
      path.join(srcDir, 'assets', 'stylesheets', s))
  }

  chapters = chapters
    .map((c, i) => ({
      filepath: c,
      index: i
    }))
  appendices = appendices
    .map((c, i) => ({
      filepath: c,
      index: i + chapters.length,
      appendix: true
    }))
  const usedsources = {}
  const cs = [
    ...chapters,
    ...appendices
  ]
  for (const c of cs) {
    const data = await readGen(c.filepath)
    Object.assign(c, data)
    Object.assign(usedsources, data.usedsources)
    const m = pages.filter(p => p.tocentry === c.index)
    if (m.length) c.pagemark = m[0]
  }

  const labels = {}
  cs.forEach(c => {
    Object.entries(c.labels).forEach(([k, v]) => {
      labels[k] = c.chapterlabel
    })
  })
  const refre = new RegExp(refPrefix + '\\{(.*?)\\}', 'g')
  cs.forEach(c => {
    c.content = c.content.replace(refre, (_, k) => labels[k])
    c.footnotes.forEach(f => {
      f.content = f.content.replace(refre, (_, k) => labels[k])
    })
  })
  if (titlepage) titlepage = await readGen(titlepage)
  if (copyright) copyright = await readGen(copyright)
  if (dedication) dedication = await readGen(dedication)
  if (foreword) foreword = await readGen(foreword)

  const dd = async (d, name) => {
    const p = path.join(srcDir, 'data', `${name}.json`)
    try {
      await fs.mkdir(path.dirname(p), { recursive: true })
    } catch (e) {
      if (e.code !== 'EEXIST') throw e
    }
    await fs.writeFile(p, JSON.stringify(d, null, 2))
  }

  const sources = Object.entries(usedsources)
    .sort()
    .map(([k, v]) => ({
      content: v
    }))
  await dd(sources, 'sources')
  const images = [
    titlepage,
    copyright,
    dedication,
    foreword,
    ...cs
  ]
    .filter(c => !!c)
    .map(c => c.images)
    .flat()
  indices = indices.map((x, i) => Object.assign({
    pages: pages.filter(p => p.index_key === x.key),
    groups: idxs[x.key].groups
  }, x))
  const indexmap = Object.fromEntries(cs
    .map(c => c.indexEntries.map(id => [id, { epubfile: c.epubfile }]))
    .flat())
  indices.forEach(i => {
    i.groups.forEach(g => {
      g.entries.forEach(e => {
        e.instances.forEach(x => {
          if (x.id in indexmap) x.epubfile = indexmap[x.id].epubfile
        })
      })
    })
  })
  await dd(indices, 'indices')

  const figures = cs
    .map(c => c.figures.map(f => Object.assign({
      chapterlabel: c.chapterlabel
    }, f)))
    .flat()
  await dd(figures, 'figures')

  const basecover = cover ? path.basename(cover) : null

  const data = {
    cover: basecover,
    title,
    subtitle,
    googleAnalytics,
    gridsomeConfig: JSON.stringify(gridsomeConfig),
    stylesheets,
    packages
  }
  await dd(data, 'metadata')
  await dd(head, 'head')
  await dd(links, 'links')

  const t = async (p, o, d = data) => {
    const template = await readTemplate(p)
    const text = template(d)
    try {
      await fs.mkdir(path.dirname(o), { recursive: true })
    } catch (e) {
      if (e.code !== 'EEXIST') throw e
    }
    await fs.writeFile(o, text)
  }
  await t('package.json', 'package.json')
  await t('gridsome.config.js', 'gridsome.config.js')
  await t('main.js', 'src/main.js')

  for (const img of images.map(i => i.path)) {
    const dest = path.join('static', 'img', img)
    await safecp(path.join(projDir, img), dest)
  }
  if (cover) {
    await fs.copyFile(path.join(projDir, cover),
      path.join('static', basecover))
  }

  if (audio) {
    const descPath = path.join('genfiles', `${description}.json`)
    const descContent = await fs.readFile(descPath, 'utf8')
    const descObj = JSON.parse(descContent)
    const rss = await generateAudioBook({
      title,
      subtitle,
      authors,
      audio,
      description: descObj,
      siteroot,
      email,
      date,
      foreword,
      chapters,
      appendices
    })
    const filepath = path.join('static', 'audiobook.rss')
    try {
      await fs.writeFile(filepath, rss)
    } catch (e) {
      console.error(`Could not write audiobook.rss: ${e.message}`)
    }
  }

  fs.writeFile(path.join('static', '_redirects'), generateRedirects({
    audio,
    redirects
  }))

  for (const filedir of filedirs) {
    const relfiledir = path.join(projDir, filedir)
    for (const fp of await glob(path.join(relfiledir, '**'), { dot: true })) {
      const rel = path.relative(relfiledir, fp)
      safecp(fp, rel)
    }
  }
}

async function install () {
  const proc = childProcess.spawn('npm', [
    'install'
  ])
  proc.stdout.on('data', data => {
    const d = data.toString().trim()
    const color = chalk.yellow
    console.log(color(d))
  })
  proc.stderr.on('data', data => {
    const d = data.toString().trim()
    console.error(chalk.red(d))
  })

  return new Promise(resolve => proc.on('exit', resolve))
}

async function gridsomeBuild () {
  const proc = childProcess.spawn('gridsome', [
    'build'
  ])
  proc.stdout.on('data', data => {
    const d = data.toString().trim()
    const color = chalk.white
    console.log(color(d))
  })
  proc.stderr.on('data', data => {
    const d = data.toString().trim()
    console.error(chalk.red(d))
  })

  return new Promise(resolve => proc.on('exit', resolve))
}

export default async (config) => {
  config = Object.assign({
    pages: [],
    gridsomeConfig: {},
    head: [],
    hooks: null,
    links: [],
    stylesheets: [],
    packages: [],
    filedirs: [],
    audio: null,
    redirects: [],
    indices: []
  }, config)
  if (isString(config.pages)) {
    const json = await fs.readFile(config.pages, 'utf8')
    try {
      config.pages = JSON.parse(json)
    } catch (e) {
      if (e instanceof SyntaxError) {
        red(`Could not parse ${config.pages}: ${e}`)
        config.pages = []
      }
      throw e
    }
  }

  const idxs = await latexIdx.readConfig(config.indices)

  const globals = {
    refPrefix: genUuid(),
    idxs
  }
  let chapternum = 0
  let appendixnum = 64
  let chapterindex = 0

  return {
    fileExt: 'json',
    remarkFilePlugin (opts) {
      this.Compiler = compiler

      const ctx = {
        chapterindex: opts.chapterindex,
        sources: opts.sources,
        refPrefix: globals.refPrefix,
        imagePrefix: '/img',
        idxs,
        labels: {},
        type: opts.type,
        chapternum,
        appendixnum
      }
      function compiler (root, file) {
        ctx.file = file
        handleNode(root, ctx)
        chapternum = ctx.chapternum
        appendixnum = ctx.appendixnum
        let id, index
        const plainfile = file.history[0].replace(/\./g, '_')
        const title = (ctx.title || plainfile).toLowerCase()
        switch (ctx.type) {
          case 'chapter':
            id = ctx.chapterlabel
              ? `c${ctx.chapterlabel}`
              : `c${title}`
            index = chapterindex++
            break
          case 'appendix':
            id = ctx.chapterlabel
              ? `a${ctx.chapterlabel}`
              : `a${title}`
            index = chapterindex++
            break
          default:
            id = ctx.type
        }
        return JSON.stringify({
          id,
          index,
          title: ctx.title,
          subtitle: ctx.subtitle,
          type: ctx.type,
          content: ctx.output,
          figures: ctx.figures,
          footnotes: ctx.footnotes,
          labels: Object.keys(ctx.labels),
          usedsources: ctx.usedsources,
          images: ctx.images,
          pages: ctx.pages,
          refPrefix: globals.refPrefix,
          indexEntries: ctx.indexEntries,
          chapterlabel: ctx.chapterlabel
        }, null, 2)
      }
    },
    async buildAfterFiles (bookConfig) {
      await replaceLabels(globals)
      await writeFiles(bookConfig, config, globals)
      const icode = await install()
      if (icode) return false
      const code = await gridsomeBuild()
      return !code
    }
  }
}
